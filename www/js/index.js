
var mobileApp = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
        this.isLoading = ko.observable();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', function(){
            this.onDeviceReady()
        }.bind(this), false);
       
    
        
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        this.getViews(function(html){

            CRM.Env.mobileInit(function(env){
                $('#loader').remove();
                ko.applyBindings(env);
            });
            
        }.bind(this)); 
    },

    getViews: function(callback) {
       
        callback = callback || $.noop;
       
        $('body').append($('<div>').load('https://dev.crmsuite.com/min.mobile.html?rand='+Math.random(),function(html){
            callback(html);
         }));
    }
};
